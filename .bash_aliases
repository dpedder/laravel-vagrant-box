#!/bin/bash

#shell
if [ -f ~/.bashrc ] 
then	
	alias ls='ls -lh --color=auto'
else
	alias ls='ls -lh'
fi

#web
alias a2reload='sudo service apache2 reload'

#vagrant 
alias vm='vagrant ssh'
alias vmd='vagrant destroy'
alias vmr='vagrant reload'
alias vmu='vagrant up'
alias vms='vagrant global-status'

#local
alias edhosts='sudo vim /etc/hosts'
