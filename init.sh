#!/bin/bash

USER_HOME=/home/vagrant

echo "---------------"
echo "--- UPDATES ---"
echo "---------------"
# Install useful utils for apt
sudo apt-get install -y software-properties-common

# Add additional repos
sudo add-apt-repository -y ppa:ondrej/php

# Update packages and upgrade
sudo apt-get update && apt-get upgrade

## SOFTWARE ##
echo "-----------"
echo "--- PHP ---"
echo "-----------"
# https://thishosting.rocks/install-php-on-ubuntu/
# Install php7.2 along with some useful packages
sudo apt-get install -y php7.2
sudo apt-get install -y php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml
update-alternatives --set php /usr/bin/php7.2

echo "-------------"
echo "--- MYSQL ---"
echo "-------------"
#install mysql - https://serversforhackers.com/video/installing-mysql-with-debconf
export DEBIAN_FRONTEND="noninteractive"

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"

sudo apt-get install -y mysql-server

echo "--------------"
echo "--- APACHE ---"
echo "--------------"
#install apache
sudo apt-get install -y apache2

#setup some mods
sudo a2enmod rewrite

#config apache's default site
sudo sed -i "s|DocumentRoot .*|DocumentRoot /var/www|" /etc/apache2/sites-available/000-default.conf

#restart apache
sudo service apache2 restart

echo "--------------"
echo "--- NODEJS ---"
echo "--------------"
# Install nodejs
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential

echo "-----------------"
echo "---- LARAVEL ----"
echo "-----------------"
# Install Composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
sudo su vagrant <<'EOF'
/usr/local/bin/composer global require "laravel/installer"
EOF

#install some useful utils
sudo apt-get install -y vim
sudo apt-get install -y git
sudo apt-get install autoconf libtool pkg-config nasm build-essential # Fixes issue with mozjpg npm module not compiling https://github.com/imagemin/imagemin-mozjpeg/issues/26

#setup sharing of the web servers root dir - /vagrant is the folder shared between host and guest.
sudo rm -rf /var/www
sudo ln -sf /vagrant /var/www

echo "-------------------"
echo "--- USER CONFIG ---"
echo "-------------------"
#add useful aliases to bash_aliases
echo "alias ls='ls -al --color=auto'" > $USER_HOME/.bash_aliases
echo "export PATH=$PATH:$USER_HOME/.composer/vendor/bin" >> $USER_HOME/.bashrc
cat << EOF >> $USER_HOME/.bashrc

parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Colours found at - https://misc.flogisoft.com/bash/tip_colors_and_formatting
# Gives us a nice looking command prompt
PS1="\[\e[33m\]\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[33m\]\h\[\e[m\]\[\e[33m\]:\[\e[m\]\[\e[33m\]\W\[\e[m\]\$(parse_git_branch) "

EOF
